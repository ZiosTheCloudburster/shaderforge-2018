﻿using System.IO;
using UnityEditor;
using UnityEngine;

namespace ShaderForge
{
	public abstract class SF_FolderReference : ScriptableObject
	{
		public string GetAssetDirectory()
		{
			return Path.GetDirectoryName(AssetDatabase.GetAssetPath(this));
		}
	}
}
