﻿using UnityEditor;
using UnityEngine;

namespace ShaderForge
{
    [CreateAssetMenu(menuName = "Shader Forge/Internal Resources Folder Reference")]
    public class SF_InternalResourcesFolderReference : SF_FolderReference
    {
        public static string GetInternalResourcesFolderPath()
        {
            string[] guids = AssetDatabase.FindAssets("l:InternalResources t:" + typeof(SF_InternalResourcesFolderReference).Name);
            if (guids == null || guids.Length < 1)
            {
                Debug.LogError("Ensure that exists at least one "+typeof(SF_InternalResourcesFolderReference).Name+" asset under the InternalResources folder.");
                return "";
            }
			
            string path = AssetDatabase.GUIDToAssetPath(guids[0]);
            SF_InternalResourcesFolderReference folderReference = AssetDatabase.LoadAssetAtPath<SF_InternalResourcesFolderReference>(path);
            return folderReference.GetAssetDirectory();
        }
    }
}
